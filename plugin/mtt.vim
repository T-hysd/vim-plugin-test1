if exists("g:loaded_vimtask")
  finish
endif
let g:loaded_vimtask = 1

let s:save_cpo = &cpo
set cpo&vim

function! s:ToggleDone(line)
  if a:line =~ '^"*\s*\[D\]'
    call setline('.', substitute(a:line, '\[D\]<.*>', '[ ]', ''))
  else
    call setline('.', substitute(a:line, '\[ \]', '[D]<'.strftime("%Y/%m/%d %H:%M").'>', ''))
  endif
endfunction

command! -nargs=0 MTT call s:ToggleDone(getline("."))

let &cpo = s:save_cpo
unlet s:save_cpo
