import neovim

@neovim.plugin
class TestNvim(object):
  def __init__(self, nvim):
    self.nvim = nvim
 
  @neovim.function("Test", sync=False)
  def test(self, str):
    with open('/tmp/log.txt','a') as f:
      self.nvim.command("echo >>> {}".format(str))
      f.write(str)
  
  @neovim.command("TestCommand", nargs='+')
  def test(self, str):
    with open('/tmp/log.txt','a') as f:
      self.nvim.command("echo2 >>> {}".format(str))
      f.write(str)
